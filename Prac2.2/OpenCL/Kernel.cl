
//TODO: set your arguments for the kernel. Note that you have to indicate if the argument is global or local. Global arguments are accessable by both host and this target device. While local can only be accessed by the device running this kernel. eg __global int* inputMatrixA, __local int* groupMemory

__kernel void matrixMultiplication(__global int* matrixA, __global int* matrixB, __global int* output ){
	
	//TODO: program your kernel here
	
	int workItemNum = get_global_id(0); //Work item ID
	int workGroupNum = get_group_id(0); //Work group ID
	int localGroupID = get_local_id(0); //Work items ID within each work group

	int A = *matrixA;
	int B = *matrixB;
	uint global_addr = workItemNum;
	
	int r = 0;
	int c = 0;
	
	if (workItemNum <= 2){
		r=0;
		c=workItemNum;
	} else if (workItemNum <= 5) {
		r=1;
		c=workItemNum-3;
	} else {
		r=2;
		c = workItemNum-6;
	}
	output[workItemNum] = matrixA[r*3]*matrixB[c] + matrixA[r*3+1]*matrixB[c+3] + matrixA[r*3+2]*matrixB[c+6];
}
